using System.Threading;

namespace APCProject
{
    public class ShellViewModel : Caliburn.Micro.Screen, IShell
    {
        private IShell _currentViewModel = new RestViewModel();

        public ShellViewModel()
        {
            if (Model.ThreadLibrary.ThreadLibrary.ControllerThread == null)
                Model.ThreadLibrary.ThreadLibrary.ControllerThread = new Thread(() => Model.Controllers.PhidgetController.Initiate(this, new RestViewModel(), new AlarmViewModel(), new NotificationViewModel()));
            if (Model.ThreadLibrary.ThreadLibrary.ControllerThread.ThreadState != ThreadState.Running)
                Model.ThreadLibrary.ThreadLibrary.ControllerThread.Start();
        }

        public IShell ViewModelToShow
        {
            get { return _currentViewModel; }
            set
            {
                _currentViewModel = value;
                NotifyOfPropertyChange(() => ViewModelToShow);
            }
        }
    }
}