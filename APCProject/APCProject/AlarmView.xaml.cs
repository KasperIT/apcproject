﻿using System.Windows;
using System.Windows.Controls;
using APCProject.Model.Controllers;

namespace APCProject
{
    public partial class AlarmView : UserControl
    {
        public AlarmView()
        {
            InitializeComponent();
        }

        private void Button_Click_Back(object sender, RoutedEventArgs e)
        {
            GoBack();
        }

        public void GoBack()
        {
            PhidgetController.ShellViewModel.ViewModelToShow = PhidgetController.RestViewModel;
        }
    }
}