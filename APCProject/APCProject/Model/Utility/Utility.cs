﻿using System;
using APCProject.Model.Controllers;

namespace APCProject.Model.Utility
{
    public class Utility
    {
        public static string TimeTo()
        {
            var preFix = "Next dose in ";

            var timeToMorning = PhidgetController.MorningLowerBoundary - PhidgetController.CurrentTime;
            var timeToMidday = PhidgetController.MiddayLowerBoundary - PhidgetController.CurrentTime;
            var timeToAfternoon = PhidgetController.AfternoonLowerBoundary - PhidgetController.CurrentTime;
            
            if (PhidgetController.CurrentTime < PhidgetController.MorningLowerBoundary)
                return preFix + timeToMorning.Hours + "h & " + timeToMorning.Minutes + "m";
            if (PhidgetController.CurrentTime < PhidgetController.MiddayLowerBoundary)
                return preFix + timeToMidday.Hours + "h & " + timeToMidday.Minutes + "m";
            if (PhidgetController.CurrentTime < PhidgetController.AfternoonLowerBoundary)
                return preFix + timeToAfternoon.Hours + "h & " + timeToAfternoon.Minutes + "m";
            if (PhidgetController.CurrentTime > PhidgetController.AfternoonLowerBoundary)
                return "Next dose Tomorrow";

            return "error";
        }
    }
}