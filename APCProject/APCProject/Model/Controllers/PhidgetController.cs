﻿using System;
using System.IO;
using System.Media;
using Phidgets;
using Phidgets.Events;
using ErrorEventArgs = Phidgets.Events.ErrorEventArgs;
using Timer = System.Timers.Timer;

namespace APCProject.Model.Controllers
{
    public static class PhidgetController
    {
        private static InterfaceKit _ik;
        private static SoundPlayer _soundPlayer;

        //ViewModels - For changing views
        public static ShellViewModel ShellViewModel;
        public static AlarmViewModel AlarmViewModel;
        public static NotificationViewModel NotificationViewModel;
        public static RestViewModel RestViewModel;

        //Checks
        private static bool _hasBoxBeenOpened;
        private static bool _timeInterval;
        private static int _notificationCounter;
        private static bool _alarm;

        //Threading
        private static Timer _intervalTimer;
        private static Timer _alarmTimer;
        private const int TimerInterval = 1000;
        private static Timer _healthCareTimer;

        //Sensor ports
        private const int MotionPort = 0;
        private const int DistPort = 1;

        //Output ports
        private const int LedNotificationPort = 7;
        private const int LedAlarmPort = 3;

        //Sensor values
        private const int MotionBoundary = 200;
        private const int MotionRest = 500;
        private const int DistBoundary = 35;

        //Time intervals
        //UpperBoundaries = UpperBoundaries - 15 minutes (due to business model: alarm 15 minutes notice before boundary-hit + healthcare notification on boundary hit
        public static readonly TimeSpan MorningLowerBoundary = new TimeSpan(7, 0, 0);
        public static readonly TimeSpan MorningUpperBoundary = new TimeSpan(8, 45, 0);
        public static readonly TimeSpan MiddayLowerBoundary = new TimeSpan(11, 30, 0);
        public static readonly TimeSpan MiddayUpperBoundary = new TimeSpan(13, 15, 0);
        public static readonly TimeSpan AfternoonLowerBoundary = new TimeSpan(18, 0, 0);
        public static readonly TimeSpan AfternoonUpperBoundary = new TimeSpan(19, 45, 0);

        private static int _timeIntervalCounter;
        private const int HealthCareIntervalTimer = 15 * 60 * 1000; // 15 minutes

        //Paths
        private static readonly string ProjectFolder = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, @"..\..\"));
        private const string NotificationSound = "Resources\\solemn.wav";
        private const string AlarmSound = "Resources\\system-fault.wav";

        public static TimeSpan CurrentTime { get; set; }
        public static bool ManualTime { get; set; }

        public static void Initiate(ShellViewModel shellViewModel, RestViewModel rest, AlarmViewModel alarm, NotificationViewModel noti)
        {
            ShellViewModel = shellViewModel;
            RestViewModel = rest;
            AlarmViewModel = alarm;
            NotificationViewModel = noti;

            _ik = new InterfaceKit();

            _ik.Attach += ik_Attach;
            _ik.Detach += ik_Detach;
            _ik.Error += ik_Error;

            _ik.SensorChange += ik_SensorChange;

            _ik.open();

            Logging.LogLibrary.Logger.Info("Waiting for Phidget attachment");
            _ik.waitForAttachment();

            if (_intervalTimer == null)
                _intervalTimer = new Timer(TimerInterval);
            _intervalTimer.Elapsed += HandleIteration;
            _intervalTimer.Start();

            if (_alarmTimer == null)
                _alarmTimer = new Timer(TimerInterval);
            _alarmTimer.Elapsed += (sender, e) => _ik.outputs[LedAlarmPort] = !_ik.outputs[LedAlarmPort];

            if (_healthCareTimer == null)
                _healthCareTimer = new Timer(HealthCareIntervalTimer);
            _healthCareTimer.Elapsed += HealthCareNotification;

            ResetToStateless();
        }

        private static void HandleIteration(object source, EventArgs e)
        {
            _intervalTimer.Stop();

            if (ManualTime)
            {
                CurrentTime = CurrentTime + new TimeSpan(0, 0, TimerInterval / 1000);
                Logging.LogLibrary.Logger.Info("ManualTime: CurrentTime = " + CurrentTime);
            }
            else
            {
                CurrentTime = DateTime.Now.TimeOfDay;
            }

            if (CurrentTime > MorningLowerBoundary && CurrentTime < MorningUpperBoundary)
            {
                _timeIntervalCounter = 1;
                _timeInterval = true;
            }

            else if (CurrentTime > MiddayLowerBoundary && CurrentTime < MiddayUpperBoundary)
            {
                _timeIntervalCounter = 2;
                _timeInterval = true;
            }

            else if (CurrentTime > AfternoonLowerBoundary && CurrentTime < AfternoonUpperBoundary)
            {
                _timeIntervalCounter = 3;
                _timeInterval = true;
            }

            else
            {
                _timeInterval = false;
                _notificationCounter = 0;
            }

            if (!_timeInterval && _timeIntervalCounter != 0)
                if (!_alarm)
                    Alarm();

            _intervalTimer.Start();
        }

        private static void ik_SensorChange(object sender, SensorChangeEventArgs e)
        {
            // Motion Sensor
            if (e.Index == MotionPort && _timeInterval)
                if (_ik.sensors[MotionPort].Value > MotionRest + MotionBoundary || _ik.sensors[MotionPort].Value < MotionRest - MotionBoundary)
                    if (_notificationCounter == 0 && _timeInterval)
                        Notification();

            // Distance Sensor
            if (e.Index == DistPort)
            {
                double lengthCentimeter;

                if (_ik.sensors[DistPort].Value >= 30 && _ik.sensors[DistPort].Value <= 580) // Original values: 80 to 490
                    lengthCentimeter = 9462 / (_ik.sensors[DistPort].Value - 16.92);
                else
                {
                    Logging.LogLibrary.Logger.Info("Distance Sensor out of bounds. Value: " + e.Value);
                    return;
                }

                if (lengthCentimeter > DistBoundary)
                {
                    _hasBoxBeenOpened = true;
                    Logging.LogLibrary.Logger.Info("_hasBoxBeenOpened = true;");
                    ResetToStateless();
                }
            }
        }

        private static void Notification()
        {
            _notificationCounter++;
            Logging.LogLibrary.Logger.Info("NotificationCounter: " + _notificationCounter);

            _ik.outputs[LedNotificationPort] = true;

            if (File.Exists(ProjectFolder + NotificationSound))
            {
                _soundPlayer = new SoundPlayer(ProjectFolder + NotificationSound);
                _soundPlayer.Play();
            }

            Logging.LogLibrary.Logger.Info("Notification");

            NotificationViewModel.StatusCenterLabel = "Medicine Notification";

            ShellViewModel.ViewModelToShow = NotificationViewModel;
        }

        private static void Alarm()
        {
            if (_hasBoxBeenOpened)
            {
                Logging.LogLibrary.Logger.Info("AlarmInterval hit - BoxHasBeenOpened. SoundAlarm(OFF)");
                _hasBoxBeenOpened = false;
                Logging.LogLibrary.Logger.Info("_hasBoxBeenOpened = false");
                ResetToStateless();
                return;
            }

            _alarm = true;

            if (File.Exists(ProjectFolder + AlarmSound))
            {
                _soundPlayer = new SoundPlayer(ProjectFolder + AlarmSound);
                _soundPlayer.PlayLooping();
            }

            _ik.outputs[LedNotificationPort] = false;

            _alarmTimer.Start();

            _healthCareTimer.Start();

            Logging.LogLibrary.Logger.Info("Alarm started");

            AlarmViewModel.StatusCenterLabel = "Take Dose Now";
            ShellViewModel.ViewModelToShow = AlarmViewModel;
        }

        private static void HealthCareNotification(object source, EventArgs e)
        {
            Logging.LogLibrary.Logger.Info("SIMULATED: The caretaker has been notified");

            _healthCareTimer.Stop();
        }

        private static void ResetToStateless()
        {
            ShellViewModel.ViewModelToShow = RestViewModel;

            _timeIntervalCounter = 0;
            _soundPlayer?.Stop();

            _ik.outputs[LedNotificationPort] = false;

            _ik.outputs[LedAlarmPort] = false;
            _alarmTimer?.Stop();
            _alarm = false;

            _healthCareTimer.Stop();

            Logging.LogLibrary.Logger.Info("ResetToStateless");
        }

        private static void ik_Error(object sender, ErrorEventArgs e)
        {
            Logging.LogLibrary.Logger.Info("Error happened: " + e.Description);
        }

        private static void ik_Detach(object sender, DetachEventArgs e)
        {
            Logging.LogLibrary.Logger.Info(e.Device + " detached.");
        }

        private static void ik_Attach(object sender, AttachEventArgs e)
        {
            Logging.LogLibrary.Logger.Info(e.Device + " attached.");
        }
    }
}