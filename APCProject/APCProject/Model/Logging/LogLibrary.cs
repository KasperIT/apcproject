﻿using NLog;

namespace APCProject.Model.Logging
{
    public class LogLibrary
    {
        public static Logger Logger = LogManager.GetCurrentClassLogger();
    }
}