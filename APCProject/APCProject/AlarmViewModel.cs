﻿using System.Timers;
using APCProject.Model.Controllers;
using APCProject.Model.Utility;
using Caliburn.Micro;

namespace APCProject
{
    public class AlarmViewModel : Screen, IShell
    {
        public AlarmViewModel()
        {
            var timer = new Timer(1000);
            timer.Elapsed += (sender, e) => Time = Time;
            timer.Elapsed += (sender, e) => TimeToDose = TimeToDose;
            timer.Start();

            StatusCenterLabel = string.Empty;
        }

        private string _statusLabel;

        public string StatusCenterLabel
        {
            get { return _statusLabel; }
            set
            {
                _statusLabel = value;
                NotifyOfPropertyChange(() => StatusCenterLabel);
            }
        }

        public string Time
        {
            get
            {
                return PhidgetController.CurrentTime.ToString(@"hh\:mm");
            }
            set
            {
                NotifyOfPropertyChange(() => Time);
            }
        }

        public string TimeToDose
        {
            get { return "Next dose in " + Utility.TimeTo(); }
            set
            {
                NotifyOfPropertyChange(() => TimeToDose);
            }
        }
    }
}
