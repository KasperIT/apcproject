﻿using System.Windows;
using System.Windows.Controls;
using APCProject.Model.Controllers;

namespace APCProject
{
    public partial class NotificationView : UserControl
    {
        public NotificationView()
        {
            InitializeComponent();
        }

        private void Button_Click_Back(object sender, RoutedEventArgs e)
        {
            GoBack();
        }

        private static void GoBack()
        {
            PhidgetController.ShellViewModel.ViewModelToShow = PhidgetController.RestViewModel;
        }
    }
}