﻿using System.Timers;
using APCProject.Model.Controllers;
using APCProject.Model.Utility;

namespace APCProject
{
    public class RestViewModel : Caliburn.Micro.Screen, IShell
    {
        public RestViewModel()
        {
            var timer = new Timer(1000);
            timer.Elapsed += (sender, e) => Time = Time;
            timer.Elapsed += (sender, e) => TimeToDose = TimeToDose;
            timer.Start();

            StatusCenterLabel = string.Empty;
        }

        private string _statusLabel;

        public string StatusCenterLabel
        {
            get { return _statusLabel; }
            set
            {
                _statusLabel = value;
                NotifyOfPropertyChange(() => StatusCenterLabel);
            }
        }

        public string Time
        {
            get
            {
                return PhidgetController.CurrentTime.ToString(@"hh\:mm");
            }
            set
            {
                NotifyOfPropertyChange(() => Time);
            }
        }

        public string TimeToDose
        {
            get { return Utility.TimeTo(); }
            set
            {
                NotifyOfPropertyChange(() => TimeToDose);
            }
        }
    }
}
