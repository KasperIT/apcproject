﻿using System;
using System.Windows;
using System.Windows.Controls;
using APCProject.Model.Controllers;

namespace APCProject
{
    public partial class RestView : UserControl
    {
        public RestView()
        {
            InitializeComponent();
            ToggleStatus.Content = PhidgetController.ManualTime;
        }

        private static void ChangeToNotificationView()
        {
            PhidgetController.ShellViewModel.ViewModelToShow = PhidgetController.NotificationViewModel;
        }

        private static void ChangeToAlarmView()
        {
            PhidgetController.ShellViewModel.ViewModelToShow = PhidgetController.AlarmViewModel;
        }

        private void Button_Click_Notification(object sender, RoutedEventArgs e)
        {
            ChangeToNotificationView();
        }

        private void Button_Click_Alarm(object sender, RoutedEventArgs e)
        {
            ChangeToAlarmView();
        }

        private void SetTimeButton_Click(object sender, RoutedEventArgs e)
        {
            PhidgetController.CurrentTime = TimeSpan.Parse(TimeNow.Text);
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            TimeNow.Text = PhidgetController.CurrentTime.Hours + ":" + PhidgetController.CurrentTime.Minutes;
        }

        private void ToggleManualClock_Click(object sender, RoutedEventArgs e)
        {
            PhidgetController.ManualTime = !PhidgetController.ManualTime;
            ToggleStatus.Content = PhidgetController.ManualTime;
        }
    }
}
